import yt
from yt.frontends.sph.data_structures import ParticleDataset
import numpy as np
from scipy import sparse
from astroML.clustering import HierarchicalClustering, get_graph_segments
from random import randint
import subprocess
import time
from PIL import Image
import socket



##This script plots Rockstar and MST clusters##

#####
#Note this script requires that "rock_update.py" is located in the same directory
#####

###
#Note that to run this script with a detached screen, it must be run in ipython
#To determine the localhost id, run the command "$echo $DISPLAY" while the screen is still detached
###


"""
Quick Notes
It seems that rockstar halo positions can be called using the command: 
    rock_set['halos','particle_position_x'] (same for y and z)
Does indexing agree across field lists?

Clusters are plotted as spheres with a radius obtained from the command:
   rock_set['halos','virial_radius'] (I think...?)
"""







screen_setup = False  #When set to true, the code functions for a detached screen in linux
ParticleDataset.filter_bbox = True
ParticleDataset._skip_cache = True
file_name = "snapshot_135.hdf5"

plot_title = ''
plot_comments = ''
time_log = []

ds0 = yt.load(file_name)
ds0.index
dd0 = ds0.all_data()
X = dd0['PartType1', 'Coordinates']
#mass = 0.01626092  #Assumes a fixed mass for all particles





##Filter by Bounding_Box##

ii = randint(0, X.shape[0]-1)
c = np.array(X[ii]).copy()
print "Center Index: %s" %str(c)
plot_title += 'c = %s, ' %str(c)
r = 20000.0  #Must be float #100 radius ---> ~30,000 particles
plot_title += 'r = %s, ' %str(r)

if r != 0.0:
    left = ds0.arr((c - (0.5*r)), 'code_length')
    right = ds0.arr((c + (0.5*r)), 'code_length')
    bbox = [[left[0], right [0]], [left[1], right[1]], [left[2], right[2]]]
    ds1 = yt.load(file_name, bounding_box = bbox)
    ds1.periodicity = (False, False, False) #Prevents particle distribution from repeating all the way back to the origin!
    dd1 = ds1.all_data()
    Y = dd1['PartType1', 'Coordinates']

else:
    ds1 = ds0
    dd1 = dd0
    Y = X
    r = float(ds0.domain_width[0])
    c = np.array([r, r, r])*0.5







##MST Boruvka Cluster Finder##

n_neighbors = 10
edge_cutoff = 0.5 #Then changed from 0.7 to 0.5!!  #Changed the edge cutoff to 0.7 from 0.9!!
cluster_cutoff = 25 #Used to be 10.... change back later....
model = HierarchicalClustering(n_neighbors=10,
                                   edge_cutoff=edge_cutoff,
                                   min_cluster_size=cluster_cutoff)


#print 'Number of particles: %s' %str(Y.shape[0])
#plot_title += '# particles: %s ' %str(Y.shape[0])
temp_time = time.time()
model.fit(Y)
time_log += ['MST: ' + str(time.time()- temp_time) + ' seconds']


            




if screen_setup == True:
    #Here we first get the localhost value by using the command "$echo $DISPLAY" before reattaching the screen
    #We then use the number specified for the following command to ensure that the localhost is correct
    k = str(int(raw_input("Done! Input localhost id number: ")))
    subprocess.os.environ['DISPLAY'] = "localhost:"+k+".0"





##Time Log##
print 'Time taken...'
for elt in time_log:
    print elt
print 'r value: ', r #REMOVE LATER
