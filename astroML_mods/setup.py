from distutils.core import setup
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("void_clusters.pyx")
)


#This is the setup script for our Cython helper function -Andrew
#Warning!!! For some reason when I compile the script the void_clusters.so file ends up in a new subfolder that looks like '.../astroML/clustering/astroML/clustering/void_clusters.so'
#To fix this manually, just pull the void_clusters.so file out of the double folders and delete the copies (They're just empty folders anyway)
