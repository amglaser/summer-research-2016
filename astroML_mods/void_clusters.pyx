
#A helper function in Cython to assist with removal of clusters below the minimum size cutoff

import numpy as np
cimport numpy as np



#DTYPE = np.int
#ctypedef np.int_t DTYPE_t


def Remove_Bad_Clusters(np.ndarray[np.int32_t, ndim=1] label, np.ndarray[np.int64_t, ndim=1] count, int min_cluster_size):
    assert label.dtype ==np.int32  and count.dtype == np.int64
    cdef int n = label.shape[0]
    cdef int i, j
    for i in range(n):
        if label[i] == -1:
            pass
        else:
            j = label[i]
            if count[j] < min_cluster_size:
                label[i] = -1

    return label