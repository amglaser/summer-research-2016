import yt
from yt.frontends.sph.data_structures import ParticleDataset
import numpy as np
from PIL import Image



ParticleDataset.filter_bbox = True
ParticleDataset._skip_cache = True
file_name = "snapshot_135.hdf5"


ds0 = yt.load(file_name)
ds0.index
dd0 = ds0.all_data()
X = dd0['PartType1', 'Coordinates']
center = np.array(X[23222])


range = 200.0
left = center - range
left = ds0.arr(left, 'code_length')
right = center + range
right = ds0.arr(right, 'code_length')
bbox = [[left[0], right [0]], [left[1], right[1]], [left[2], right[2]]]

ds1 = yt.load(file_name, bounding_box = bbox)
dd1 = ds1.all_data()
Y = dd1['PartType1', 'Coordinates']


print 'Total: ', ds0.domain_width
print 'Restricted: ', ds1.domain_width
print 'Total Particles: ', X.shape[0]
print 'Total Particles in Region: ', Y.shape[0]

"""
weirdos = 0
#max_dist = []
for elt in Y:
    #print left < np.array(elt), np.array(elt) < right
    if (False in (left < elt)) or (False in (elt < right)):
        #print "Uh oh!"
        #print left, elt, right
        weirdos += 1
        #max_dist += [np.linalg.norm(np.array(elt) - c)]

print "Total: " + str(Y.shape[0])  
print "Weirdos: " + str(weirdos)
#print "Max: " + str(max(max_dist))
"""


##Projection Plot

def image_update(plot, filename): #Note, filename only specifies what the file names start with for multiple plots
    files = plot.save(filename)
    for elt in files:
        img = Image.open(elt)
        img.show()



p = yt.ProjectionPlot(ds1, "z", "PartType1_density")
image_update(p, "snip_test")
