import yt
from yt.frontends.sph.data_structures import ParticleDataset
import numpy as np
from matplotlib import pyplot as plt #REMOVE?
from matplotlib.axes import Axes #REMOVE?
from scipy import sparse
from astroML.clustering import HierarchicalClustering, get_graph_segments
from yt.analysis_modules.halo_finding.api import * #Is this using an old model?
from random import randint
import subprocess
import time
from PIL import Image
import socket



##This script plots Rockstar and MST clusters##

#####
#Note this script requires that "rock_update.py" is located in the same directory
#####

###
#Note that to run this script with a detached screen, it must be run in ipython
#To determine the localhost id, run the command "$echo $DISPLAY" while the screen is still detached
###


"""
Quick Notes
It seems that rockstar halo positions can be called using the command: 
    rock_set['halos','particle_position_x'] (same for y and z)
Does indexing agree across field lists?

Clusters are plotted as spheres with a radius obtained from the command:
   rock_set['halos','virial_radius'] (I think...?)
"""







screen_setup = True #When set to true, the code functions for a detached screen in linux
ParticleDataset.filter_bbox = True
ParticleDataset._skip_cache = True
file_name = "snapshot_135.hdf5"

plot_title = ''
plot_comments = ''
time_log = []

ds0 = yt.load(file_name)
ds0.index
dd0 = ds0.all_data()
X = dd0['PartType1', 'Coordinates']
#mass = 0.01626092  #Assumes a fixed mass for all particles





##Filter by Bounding_Box##

ii = randint(0, X.shape[0]-1)
c = np.array(X[ii]).copy()
print "Center Index: %s" %str(c)
plot_title += 'c = %s, ' %str(c)
r = 30000.0  #Must be float #100 radius ---> ~30,000 particles
plot_title += 'r = %s, ' %str(r)

if r != 0.0:
    left = ds0.arr((c - (0.5*r)), 'code_length')
    right = ds0.arr((c + (0.5*r)), 'code_length')
    bbox = [[left[0], right [0]], [left[1], right[1]], [left[2], right[2]]]
    ds1 = yt.load(file_name, bounding_box = bbox)
    ds1.periodicity = (False, False, False) #Prevents particle distribution from repeating all the way back to the origin!
    dd1 = ds1.all_data()
    Y = dd1['PartType1', 'Coordinates']

else:
    ds1 = ds0
    dd1 = dd0
    Y = X
    r = float(ds0.domain_width[0])
    c = np.array([r, r, r])*0.5







##MST Boruvka Cluster Finder##

n_neighbors = 10
edge_cutoff = 0.5 #Then changed from 0.7 to 0.5!!  #Changed the edge cutoff to 0.7 from 0.9!!
cluster_cutoff = 25 #Used to be 10.... change back later....
model = HierarchicalClustering(n_neighbors=10,
                                   edge_cutoff=edge_cutoff,
                                   min_cluster_size=cluster_cutoff)


#print 'Number of particles: %s' %str(Y.shape[0])
#plot_title += '# particles: %s ' %str(Y.shape[0])
temp_time = time.time()
model.fit(Y)
time_log += ['MST: ' + str(time.time()- temp_time) + ' seconds']
#print 'Number of MST Clusters: %s' %str(model.n_components_)
#plot_comments += 'MST Clusters: %s, '  %str(model.n_components_)


#labels = list(model.labels_)
#L1 = [] #Count of MST cluster sizes
#for i in range(model.n_components_):
#    L1 += [labels.count(i)]





"""

##MST 0.70 edge cutoff version## COPY COPY COPY !!!


#We just adjust the edge_cutoff in the model...
temp_time = time.time()
mod_update_7 = model.compute_clusters(edge_cutoff = 0.7)
time_log += ['MST 0.7 ver: ' + str(time.time()- temp_time) + ' seconds']
#Returns a list with 3 entries
#0th entry is the new value of model.n_components_
#1st entry is the new model.labels_ list
#2nd entry is the new truncated minimum spanning tree
##NOTE that the values of the model itself are not updated!!!

print 'Number of MST .7 Clusters: ', mod_update_7[0]
plot_comments += 'MST (0.7): %s, '  %str(mod_update_7[0])

labels_7 = list(mod_update_7[1])
#L7 = [] #Count of MST cluster sizes
#for i in range(mod_update_7[0]):
#    L7 += [labels_7.count(i)]

"""


##Rockstar Cluster Finder##

temp_time = time.time()
pop = subprocess.Popen("mpirun -n 3 python rock_update.py" + " " + file_name + " " + str(r) + " " + str(c[0]) + " "  + str(c[1]) + " " + str(c[2]), shell = True)
pop.communicate() #Waits for the subprocess to complete in terminal
time_log += ['(Approx) Rockstar: ' + str(time.time()- temp_time) + ' seconds'] #Likely some additional time wasted due to openning a new file and initializing mpi, yt parallelism, etc...


temp_host = socket.gethostname()
host = temp_host.split('.')[0]
rockstar_files = host + "/rockstar_halos/halos_0.0.bin"
rock_halos = yt.load(rockstar_files) #This is the default file where the halo data is saved
rock_set = rock_halos.all_data() #Dataset of rockstar calculated halos 
#L4 = list(np.array(rock_set['halos','num_child_particles']))
#print 'Rockstar Clusters: %s' %str(len(L4))
#plot_comments += 'Rockstar Clusters: %s'  %str(len(L4))







############Plotting###################
#######################################

def image_update(plot, filename): #Shows the new image and overwrites the same image file
    files = plot.save(filename)
    for elt in files:
        img = Image.open(elt)
        img.show()





#Create a projection plot with the halos overplot on top
#3 copies so we can see the different layers of annotations
p1 = yt.ProjectionPlot(ds1, "x", "PartType1_density") #Note that this index, "PartType1_density",  may not be accurate for other datasets!!!
p2 = yt.ProjectionPlot(ds1, "x", "PartType1_density")
p3 = yt.ProjectionPlot(ds1, "x", "PartType1_density")
p4 = yt.ProjectionPlot(ds1, "x", "PartType1_density")

print 'Done setting up three plot copies!'


#Manually plot the rockstar clusters for the projection plot
units = ds1.length_unit.units
rescale = float(ds1.domain_width[0].in_units(units))/float(rock_halos.domain_width[0].in_units(units))
temp_rad = rock_set['halos','virial_radius'].in_units(units)*rescale #Should I copy this into a new array???
temp_x = ds1.arr(((np.array(rock_set['halos', 'particle_position_x'].in_units(units))*rescale) + (c[0] - (0.5*r))), 'code_length')
temp_y = ds1.arr(((np.array(rock_set['halos', 'particle_position_y'].in_units(units))*rescale) + (c[1] - (0.5*r))), 'code_length')
temp_z =  ds1.arr(((np.array(rock_set['halos', 'particle_position_z'].in_units(units))*rescale) + (c[2] - (0.5*r))), 'code_length')

for i in range(temp_rad.shape[0]):
    p2.annotate_sphere([temp_x[i], temp_y[i], temp_z[i]], radius=temp_rad[i], coord_system='data', circle_args={'color':'black'})
    p3.annotate_sphere([temp_x[i], temp_y[i], temp_z[i]], radius=temp_rad[i], coord_system='data', circle_args={'color':'black'})


print 'Done plotting Rockstar Halos!'


"""
#Manually plot boruvka MST for the projection plot
T_trunc_x, T_trunc_y = get_graph_segments((model.X_train_[:,0:2]), model.cluster_graph_)
T_trunc_y, T_trunc_z = get_graph_segments((model.X_train_[:,1:3]), model.cluster_graph_)

for i in range(len(T_trunc_x[0])):
     p3.annotate_line([T_trunc_x[0][i], T_trunc_y[0][i], T_trunc_z[0][i]], [T_trunc_x[1][i], T_trunc_y[1][i], T_trunc_z[1][i]], coord_system='data', plot_args={'color':'white'})
"""





##Calculating MST Virial Radii##


#Creates a dictionary containing the particle positions in each cluster
vir_dict = {}
for i in range(model.n_components_):
    vir_dict[i] = []

for i in range(model.X_train_.shape[0]):
    if model.labels_[i] == -1:
        pass
    else:
        j = model.labels_[i]
        vir_dict[j] += [list(model.X_train_[i,:])]




#Sphere MST cluster plotting
mass =  4.61906787*(10**38)    # 0.01626092  #Assumes a fixed mass for all particles !!! WARNING MUST BE UPDATED FOR OTHER DATASETS!!!! <--- Make this more robust later!
for i in range(model.n_components_):
    
    c_mass_x = 0
    c_mass_y = 0
    c_mass_z = 0
    total_mass = 0
    for j in range(len(vir_dict[i])):
        c_mass_x += vir_dict[i][j][0]*mass
        c_mass_y += vir_dict[i][j][1]*mass
        c_mass_z += vir_dict[i][j][2]*mass
        total_mass += mass
    
    c_mass_x = c_mass_x/total_mass
    c_mass_y = c_mass_y/total_mass
    c_mass_z = c_mass_z/total_mass

    for j in range(len(vir_dict[i])):
        vir_dict[i][j][0] -= c_mass_x
        vir_dict[i][j][1] -= c_mass_y
        vir_dict[i][j][2] -= c_mass_z
        vir_dict[i][j] += [np.linalg.norm(vir_dict[i][j])]
    
    
    vir_dict[i] = sorted(vir_dict[i], key = lambda x:x[-1]) #Sorted least to greatest distance

    vir_radius = vir_dict[i][-1][-1]
    CUTOFF = 200.0* 9.47* (10.0**(-27))  #This is the R200c virial radius  <---- double check this critical density (of the universe) value!!!
    quick_stop = False
    while (total_mass/((4.0/3.0)*np.pi*(vir_radius**3)) < CUTOFF) and (not quick_stop):
        print vir_dict[i] #REMOVE
        test = raw_input('bleep bloop') #REMOVE
        if test == 'q': #REMOVE
            quit #REMOVE
        vir_dict[i] = vir_dict[i][:-1]
        total_mass -= mass
        vir_radius = vir_dict[i][-1][-1]
        
        if len(vir_dict[i]) == 0:
            print 'Oh no! A cluster never satisfied the density cutoff!'
            quick_stop = True

        if (4.0/3.0)*np.pi*(vir_radius**3) == 0:
            print "Weird, we have a virial radius of zero..."
            quick_stop = True

    if not quick_stop:
        p3.annotate_sphere([c_mass_x, c_mass_y, c_mass_z], radius=vir_radius, coord_system='data', circle_args={'color':'white'})
        p4.annotate_sphere([c_mass_x, c_mass_y, c_mass_z], radius=vir_radius, coord_system='data', circle_args={'color':'white'})
            




if screen_setup == True:
    #Here we first get the localhost value by using the command "$echo $DISPLAY" before reattaching the screen
    #We then use the number specified for the following command to ensure that the localhost is correct
    k = str(int(raw_input("Done! Input localhost id number: ")))
    subprocess.os.environ['DISPLAY'] = "localhost:"+k+".0"


temp_host = socket.gethostname()
host = temp_host.split('.')[0]
image_update(p1, host + "_test_clust_v1")
image_update(p2, host + "_test_clust_v2")
image_update(p3, host + "_test_clust_v3")
image_update(p4, host + "_test_clust_v4")




##Time Log##
print 'Time taken...'
for elt in time_log:
    print elt
print 'r value: ', r #REMOVE LATER
