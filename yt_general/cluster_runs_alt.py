import yt
from yt.frontends.sph.data_structures import ParticleDataset
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.axes import Axes #REMOVE?
from scipy import sparse
from astroML.clustering import HierarchicalClustering
from yt.analysis_modules.halo_finding.api import * #Is this using an old model?
from random import randint
import subprocess
import socket
import time

#####
#Note this script requires that "rock_update.py" is located in the same directory
#####

###
#Note that to run this script with a detached screen, it must be run in ipython
#To determine the localhost id, run the command "$echo $DISPLAY" while the screen is still detached
###




screen_setup = True #When set to true, the code functions for a detached screen in linux
ParticleDataset.filter_bbox = True
ParticleDataset._skip_cache = True
file_name = "snapshot_135.hdf5"

plot_title = ''
plot_comments = ''
time_log = []

ds0 = yt.load(file_name)
ds0.index
dd0 = ds0.all_data()
X = dd0['PartType1', 'Coordinates']






##Filter by Bounding_Box##

ii = randint(0, X.shape[0]-1)
c = np.array(X[ii]).copy()
print "Center Index: %s" %str(c)
plot_title += 'c = %s, ' %str(c)
r = 20000.0  #Must be float #100 radius ---> ~30,000 particles
plot_title += 'r = %s, ' %str(r)

if r != 0.0:
    left = ds0.arr((c - (0.5*r)), 'code_length')
    right = ds0.arr((c + (0.5*r)), 'code_length')
    bbox = [[left[0], right [0]], [left[1], right[1]], [left[2], right[2]]]
    ds1 = yt.load(file_name, bounding_box = bbox)
    ds1.periodicity = (False, False, False) #Prevents particle distribution from repeating all the way back to the origin!
    dd1 = ds1.all_data()
    Y = dd1['PartType1', 'Coordinates']

else:
    ds1 = ds0
    dd1 = dd0
    Y = X
    r = float(ds0.domain_width[0])
    c = np.array([r,r,r])*0.5

mass = dd1['PartType1','Masses'].in_units('kg')[0] #WARNING !!! Assumes a constant mass for all particles





##MST Boruvka Cluster Finder##

n_neighbors = 10
edge_cutoff = 0.9
cluster_cutoff = 10
model = HierarchicalClustering(n_neighbors=10,
                                   edge_cutoff=edge_cutoff,
                                   min_cluster_size=cluster_cutoff)


print 'Number of particles: %s' %str(Y.shape[0])
plot_title += '# particles: %s ' %str(Y.shape[0])
temp_time = time.time()
model.fit(Y)
time_log += ['MST: ' + str(time.time()- temp_time) + ' seconds']
print 'Number of MST Clusters: %s' %str(model.n_components_)
plot_comments += 'MST Clusters: %s, '  %str(model.n_components_)


labels = list(model.labels_)
L1 = [] #Count of MST cluster sizes
for i in range(model.n_components_):
    L1 += [labels.count(i)]




"""
#Quick sanity check
temp_sum = 0
for i in range(clust_size_freq.size):
    temp_sum += i*clust_size_freq[i]
print clust_size_freq
print temp_sum + L.count(-1)
print Y.shape[0]


temp = ''
for i in range(clust_size_freq.size):
    if clust_size_freq[i] != 0.0:
        print i, clust_size_freq[i]
        temp = raw_input('')
    if temp == 'q':
        break
"""







##MST 0.95 edge cutoff## COPY COPY COPY WARNING OVERWRITES PREVIOUS MST MODEL !!!

"""
#This builds a new version from scratch

n_neighbors = 10
edge_cutoff = 0.95
cluster_cutoff = 10
model = HierarchicalClustering(n_neighbors=10,
                               edge_cutoff=edge_cutoff,
                               min_cluster_size=cluster_cutoff)


model.fit(Y)
print 'Number of MST .95 Clusters: %s' %str(model.n_components_)
plot_comments += 'MST 0.95 Clusters: %s, '  %str(model.n_components_)

labels = list(model.labels_)
L5 = [] #Count of MST cluster sizes
for i in range(model.n_components_):
    L5 += [labels.count(i)]
"""


#Alternatively, we just adjust the edge_cutoff in the model...
temp_time = time.time()
mod_update = model.compute_clusters(edge_cutoff = 0.95)
time_log += ['MST 0.95 ver: ' + str(time.time()- temp_time) + ' seconds']
#Returns a list with 3 entries
#0th entry is the new value of model.n_components_
#1st entry is the new model.labels_ list
#2nd entry is the new truncated minimum spanning tree
##NOTE that the values of the model itself are not updated!!!

print 'Number of MST .95 Clusters: ', mod_update[0]
#plot_comments += 'MST 0.95 ver: %s, '  %str(mod_update[0]) #Not enough screen room

labels = list(mod_update[1])
L5 = [] #Count of MST cluster sizes
for i in range(mod_update[0]):
    L5 += [labels.count(i)]






##MST 0.85 edge cutoff version## COPY COPY COPY !!!


#We just adjust the edge_cutoff in the model...
temp_time = time.time()
mod_update_85 = model.compute_clusters(edge_cutoff = 0.85)
time_log += ['MST 0.85 ver: ' + str(time.time()- temp_time) + ' seconds']
#Returns a list with 3 entries
#0th entry is the new value of model.n_components_
#1st entry is the new model.labels_ list
#2nd entry is the new truncated minimum spanning tree
##NOTE that the values of the model itself are not updated!!!

print 'Number of MST .85 Clusters: ', mod_update_85[0]
plot_comments += 'MST (0.85): %s, '  %str(mod_update_85[0])

labels_85 = list(mod_update_85[1])
L6 = [] #Count of MST cluster sizes
for i in range(mod_update_85[0]):
    L6 += [labels_85.count(i)]






##MST 0.70 edge cutoff version## COPY COPY COPY !!!


#We just adjust the edge_cutoff in the model...
temp_time = time.time()
mod_update_7 = model.compute_clusters(edge_cutoff = 0.7)
time_log += ['MST 0.7 ver: ' + str(time.time()- temp_time) + ' seconds']
#Returns a list with 3 entries
#0th entry is the new value of model.n_components_
#1st entry is the new model.labels_ list
#2nd entry is the new truncated minimum spanning tree
##NOTE that the values of the model itself are not updated!!!

print 'Number of MST .7 Clusters: ', mod_update_7[0]
plot_comments += 'MST (0.7): %s, '  %str(mod_update_7[0])

labels_7 = list(mod_update_7[1])
L7 = [] #Count of MST cluster sizes
for i in range(mod_update_7[0]):
    L7 += [labels_7.count(i)]






##HOP Cluster Finder##

temp_time = time.time()
hops = HOPHaloFinder (ds = ds1)
time_log += ['HOP: ' + str(time.time()- temp_time) + ' seconds']
print 'Number of HOP Clusters: %s' %str(len(hops))
plot_comments += 'HOP Clusters: %s, '  %str(len(hops))
L2 = [] #Count of HOP cluster sizes
for elt in hops:
    L2 += [elt.get_size()]



##FoF Cluster Finder##

temp_time = time.time()
fofs = FOFHaloFinder(ds = ds1)
time_log += ['FoF: ' + str(time.time()- temp_time) + ' seconds']
print 'Number of FoF Clusters: %s' %str(len(fofs))
plot_comments += 'FoF Clusters: %s, '  %str(len(fofs))
L3 = [] #Count of FoF cluster sizes
for elt in fofs:
    L3 += [elt.get_size()]




##Rockstar Cluster Finder##

temp_time = time.time()
pop = subprocess.Popen("mpirun -n 3 python rock_update.py" + " " + file_name + " " + str(r) + " " + str(c[0]) + " "  + str(c[1]) + " " + str(c[2]), shell = True)
pop.communicate() #Waits for the subprocess to complete in terminal
time_log += ['(Approx) Rockstar: ' + str(time.time()- temp_time) + ' seconds'] #Likely some additional time wasted due to openning a new file and initializing mpi, yt parallelism, etc...


temp_host = socket.gethostname()
host = temp_host.split('.')[0]
rockstar_files = host + "/rockstar_halos/halos_0.0.bin"
rock_halos = yt.load(rockstar_files) #This is the default file where the halo data is saved
rock_set = rock_halos.all_data() #Dataset of rockstar calculated halos 
L4 = []
for i in range(rock_set['halos','particle_mass'].shape[0]):
    L4 += [round(rock_set['halos','particle_mass'].in_units('kg')[i]/mass)]
print 'Rockstar Clusters: %s' %str(len(L4))
plot_comments += 'Rockstar Clusters: %s'  %str(len(L4))





############Plotting###################
#######################################


def clust_counter(L):#L is a list of all the cluster sizes
    clust_size_freq = np.zeros(Y.shape[0]+1) #Each +1 is one more distinct cluster containing the index # of elements
    for i in set(L):###Could our cluster counting algorithm be off? Or double counting somehow???? <----- CHECK THIS OUT LATER
        j = L.count(i)
        clust_size_freq[i] += j

    #Number of clusters less than N (index value)
    cum_size_freq = np.zeros(clust_size_freq.size)
    temp_sum = 0
    for i in range(clust_size_freq.size -1, -1, -1): #This might be unecessarily time consuming?
        temp_sum += clust_size_freq[i]
        cum_size_freq[i] = temp_sum

    return clust_size_freq, cum_size_freq #Returns frequencies and cumulative frequencies



def shift_view(event):
    
    global xmin
    global xmax
    global delta
    global ax
    
    
    if event.key == 'q':
        plt.close()

    if event.key == 'left':
        xmin -= delta
        xmax -= delta
        plt.xlim(xmin, xmax)
        plt.draw()

    if event.key == 'right':
        xmin += delta
        xmax += delta
        plt.xlim(xmin, xmax)
        plt.draw()





L1a, L1b = clust_counter(L1) #Baruvka clust size freq & cum size freq
L2a, L2b = clust_counter(L2) #HOP clust size ...etc...
L3a, L3b = clust_counter(L3) #FoF clust size ...etc...
L4a, L4b = clust_counter(L4) #Rockstar clust size ...etc...
L5a, L5b = clust_counter(L5) #MST 0.95 edge cuttoff clust size ...etc...
L6a, L6b = clust_counter(L6) #MST 0.85 edge cuttoff clust size ...etc...
L7a, L7b = clust_counter(L7) #MST 0.70 edge cuttoff clust size ...etc...




Lmax = [L1b, L2b, L3b, L4b, L5b, L6b, L7b] #Finds the largest cluster size
Lmaxed = [0] #In case all lists contain only zeros
for elt in Lmax:
    if elt.nonzero()[0].size!=0:
        Lmaxed += [elt.nonzero()[0][-1]]
M = max(Lmaxed) #Largest cluster size overall


xmin = 0
xmax = 0.2*M
delta = (1.0/3.0)*0.2*M



if screen_setup == True:
    #Here we first get the localhost value by using the command "$echo $DISPLAY" before reattaching the screen
    #We then use the number specified for the following command to ensure that the localhost is correct
    k = str(int(raw_input("Done! Input localhost id number: ")))
    subprocess.os.environ['DISPLAY'] = "localhost:"+k+".0"



#Plot of cluster size
fig, ax = plt.subplots()
fig.canvas.mpl_connect('key_press_event', shift_view)
N = np.array(range(Y.shape[0]+1)) 
plt.plot(N, L2b, color = 'g', label = 'HOP') #For fixed mass, N is proportional to total mass of each cluster
plt.plot(N, L3b, color = 'k', label = 'FoF')
plt.plot(N, L4b, color = 'r', label = 'Rockstar')
plt.plot(N, L1b, color = 'b', linestyle = 'dashed', label = 'Boruvka MST')
plt.plot(N, L5b, color = 'r', linestyle = 'dashed', label = '0.95 MST')
plt.plot(N, L6b, color = 'g', linestyle = 'dashed', label = '0.85 MST')
plt.plot(N, L7b, color = 'k', linestyle = 'dashed', label = '0.70 MST')
plt.legend()
plt.suptitle(plot_title, fontweight = 'bold', fontsize = 24)
plt.title(plot_comments, fontweight = 'bold', fontsize = 24)
plt.xlabel('log N')
plt.ylabel('Num Clusters w/ Size $\geq$ N')
plt.xscale('log')
plt.show(block=False)


xmin = 0
xmax = 0.2*M
fig, ax = plt.subplots()
ax.autoscale(axis = 'y')
fig.canvas.mpl_connect('key_press_event', shift_view)
#plt.xlim(xmin, xmax)
plt.plot(N, L2b, color = 'g', label = 'HOP')
plt.plot(N, L3b, color = 'k', label = 'FoF')
plt.plot(N, L4b, color = 'r', label = 'Rockstar')
plt.plot(N, L1b, color = 'b', linestyle = 'dashed', label = 'Boruvka MST') 
plt.plot(N, L5b, color = 'r', linestyle = 'dashed', label = '0.95 MST')
plt.plot(N, L6b, color = 'g', linestyle = 'dashed', label = '0.85 MST')
plt.plot(N, L7b, color = 'k', linestyle = 'dashed', label = '0.70 MST')
plt.legend()
plt.suptitle(plot_title, fontweight = 'bold', fontsize = 24)
plt.title(plot_comments, fontweight = 'bold', fontsize = 24)
plt.xlabel('N')
plt.ylabel('Num Clusters w/ Size $\geq$ N ')
plt.show(block=False)





##Time Log##
print 'Time taken...'
for elt in time_log:
    print elt



"""
#Crude cluster size list investigator

temp = ''
for i in range(L1a.size):
    if L1a[i]!=0:
        print i, L1a[i]
        temp = raw_input("")
    
    if temp == 'q':
        break


for i in range(L2a.size):
    if L2a[i]!=0:
        print i, L2a[i]
        temp = raw_input("")

    if temp == 'q':
        break


"""
