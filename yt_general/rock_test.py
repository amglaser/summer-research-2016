import yt
from yt.frontends.sph.data_structures import ParticleDataset
import numpy as np
from PIL import Image
import mpi4py as MPI
from yt.analysis_modules.halo_finding.rockstar.api import *

"""
yt.enable_parallelism()

ParticleDataset.filter_bbox = True
ParticleDataset._skip_cache = True
file_name = "snapshot_135.hdf5"


ds0 = yt.load(file_name)
ds0.index
dd0 = ds0.all_data()
X = dd0['PartType1', 'Coordinates']
center = np.array(X[23222])


range = 200.0
left = center - range
left = ds0.arr(left, 'code_length')
right = center + range
right = ds0.arr(right, 'code_length')
bbox = [[left[0], right [0]], [left[1], right[1]], [left[2], right[2]]]

ds1 = yt.load(file_name, bounding_box = bbox)
ds1.periodicity = (False, False, False)
dd1 = ds1.all_data()
#ts = TimeSeries([ds1])

print 'Total: ', ds0.domain_width
print 'Restricted: ', ds1.domain_width
print 'Total Particles: ', X.shape[0]

rocs = RockstarHaloFinder(ds1)
rocs.run() #Overwrites previous rockstar data!
"""
if yt.is_root():
    print "Yay! No errors!"
    rock_halos = yt.load('rockstar_halos/halos_0.0.bin')
    rock_set = rock_halos.all_data()
    L4 = np.array(rock_set['halos','num_child_particles'])
    print L4
    print 'Number of Rockstar Halos: ', L4.shape[0]
    
    
