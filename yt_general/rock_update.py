import yt
from yt.frontends.sph.data_structures import ParticleDataset
import numpy as np
from scipy import sparse
from yt.analysis_modules.halo_finding.rockstar.api import * ##Keep this?
import mpi4py as MPI
import sys
import socket


#This is the assistant file for cluster_runs.py#

yt.enable_parallelism()


file_name, r, x, y, z = sys.argv[1:]
c = np.array([float(x), float(y), float(z)]) #center
r = float(r) #box width



ParticleDataset.filter_bbox = True
ParticleDataset._skip_cache = True
ds0 = yt.load(file_name)



####Filter by Bounding_Box####

if r != 0.0:
    left = ds0.arr((c - (0.5*r)), 'code_length')
    right = ds0.arr((c + (0.5*r)), 'code_length')
    bbox = [[left[0], right [0]], [left[1], right[1]], [left[2], right[2]]]
    ds1 = yt.load(file_name, bounding_box = bbox)
    ds1.periodicity = (False, False, False)

else:
    ds1 = ds0


##Rockstar Clustering##
temp_host = socket.gethostname()
host = temp_host.split('.')[0]
out_folder = host + "/rockstar_halos"


rocs = RockstarHaloFinder(ds1, outbase = out_folder)
rocs.run() #Overwrites previous rockstar data!

if yt.is_root():
    print 'Rockstar Update Complete!'
