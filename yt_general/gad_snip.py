import yt
from yt.frontends.sph.data_structures import ParticleDataset
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt


ParticleDataset.filter_bbox = True
ParticleDataset._skip_cache = True
file_name = "GadgetDiskGalaxy/snapshot_200.hdf5"


ds0 = yt.load(file_name)
ds0.index
dd0 = ds0.all_data()
X = dd0['PartType1', 'Coordinates']
center = np.array(X[23222])


range = 20.0
left = center - range
left = ds0.arr(left, 'code_length')
right = center + range
right = ds0.arr(right, 'code_length')
bbox = [[left[0], right [0]], [left[1], right[1]], [left[2], right[2]]]
print "Box:", bbox

ds1 = yt.load(file_name, bounding_box = bbox)
ds1.periodicity = (False, False, False)
dd1 = ds1.all_data()
Y = dd1['PartType1', 'Coordinates']


print 'Total: ', ds0.domain_width
print 'Restricted: ', ds1.domain_width
print 'Total Particles: ', X.shape[0]
print 'Total Particles in Region: ', Y.shape[0]


plt.scatter(Y[:,0], Y[:,1])
plt.show()



"""

##Projection Plot

def image_update(plot, filename): #Shows the new image and overwrites the same image file
    files = plot.save(filename)
    for elt in files:
        img = Image.open(elt)
        img.show()



p1 = yt.ProjectionPlot(ds1, "z", "PartType1_density")
image_update(p1, "gad_test")




bbox[0][0] = ds0.quan(0.0)#Sets the lower region of the box to the origin
bbox[1][0] = ds0.quan(0.0)
bbox[2][0] = ds0.quan(0.0)

ds2 = yt.load(file_name, bounding_box = bbox)
p2 = yt.ProjectionPlot(ds2, "z", "PartType1_density")
image_update(p2, "gad_test")
"""
