import numpy as np
from astroML.clustering import HierarchicalClustering


def Adaptive_MST(X, n=2): #Accepts a numpy array, X, of position values for particles in a given dataset
    labels_dict = {} #Dictionary of cluster labels indexed by the edge_cutoff percentage
    labels_count1 = {} #Same dictionary keys, that contain corresponding bincounts, NOTE index is increased by 1!!!
    n_neighbors = 10
    edge_cutoff = 0.9
    cluster_cutoff = 25
    model = HierarchicalClustering(n_neighbors=n_neighbors,
                                       edge_cutoff=edge_cutoff,
                                       min_cluster_size=cluster_cutoff)

    model.fit(X)
    labels_dict[90] = model.labels_.copy()
    labels_count1[90] = np.bincount(model.labels_.copy() + 1)
    for i in range(90-n, 0, -n): #<--- Note, this approach to find the initial cutoff value seems inefficient!!!
        p = float(i)/100.0
        labels_dict[i] = model.compute_clusters(edge_cutoff = p)[1]
        labels_count1[i] = np.bincount(labels_dict[i].copy() +1)

    N = float(X.shape[0])
    top = 70 #min(labels_count1, key=lambda x: abs((float(labels_count1[x][0])/N) - 0.7) ) #This will be our initial MST cutoff level #Trying 70% edge cutoff overall since at first Rockstar only uses a simple FoF for the highest cluster structure
    print 'Original Edge Cutoff is '+ str(top) + '%' #REMOVE LATER?
    
    
    X = X.copy()
    indx = labels_dict[top].argsort()
    X = X[indx, :]
    for key in labels_dict: #WARNING only keys below the designated "top" value are sorted!!!
        if key <= top:
            labels_dict[key] = labels_dict[key][indx]


    temp = np.empty(N, dtype=int)
    temp.fill(-1)
    subhalo_labels ={}
    for i in range(4):
        subhalo_labels[i] = temp.copy()



    k = 0
    while labels_dict[top][k] == -1:
        k +=1
        if k == N:
            print "Error! No clusters found in top label list!"
            break
    
    i_lo = k
    current = labels_dict[top][k]
    for i in range(k, int(N)):
        if current == labels_dict[top][i]:
            pass

        else:
            i_hi = i-1
            MST_helper(labels_dict, labels_count1, subhalo_labels, top, i_lo, i_hi, n)
            i_lo = i
            current = labels_dict[top][i]


    return X, labels_dict[top], subhalo_labels[0], subhalo_labels[1], subhalo_labels[2], subhalo_labels[3]


#test = '' #REMOVE

def MST_helper(labels_dict, count_dict, subhalo_dict, top, lo, hi, n):#Investigates all layers/refinements for subhalos in given region
    size = float(hi - lo + 1)
    level = 0
    p = 0.7
    lo_cutoff = size* (p - 0.03) #This might be too narrow of a criteria...??? <--- Investigate later
    hi_cutoff = size* (p + 0.03)
    N = labels_dict[top].shape[0]
    #global test #REMOVE
    for i in range(top-n, 0, -n):
        x_lo = N + 1
        x_hi = labels_dict[i][lo]
        for j in range(lo, hi+1):
            if (labels_dict[i][j] != -1):
                if labels_dict[i][j] > x_hi:
                    x_hi = labels_dict[i][j]
                if labels_dict[i][j] < x_lo:
                    x_lo = labels_dict[i][j]

        if (x_lo == N + 1) and (x_hi == -1):
            return
        else:
            max = [count_dict[i][x_lo+1], x_lo]
            for k in range(x_lo+1, x_hi +2):
                if count_dict[i][k] > max[0]:
                    max = [count_dict[i][k], k-1]
            
            if max[0] < lo_cutoff:
                return

            if max[0] <= hi_cutoff:
                first = True
                size = 0
                #print 'Yaaaaaay' #REMOVE
                #print "level: ", level #REMOVE
                #print "edge cutoff: ", i #REMOVE
                #if test != 'q': #REMOVE
                #    test = raw_input('') #REMOVE
                #print 'range: ', lo, ' ',  hi #REMOVE
                #print 'max: ', max[1] #REMOVE
                for j in range(lo, hi+1):
                    if labels_dict[i][j] == max[1]:
                        subhalo_dict[level][j] = (level+1)*N + max[1]
                        size += 1
                        if first:
                            temp_lo = j
                            temp_hi = j
                            first = False
                        else:
                            temp_hi = j

                if not first: #Note it seems that sometimes the "if labels_dict[i][j] ==..." is never being hit in the loop above... this is should never happen <---- INVESTIGATE ???
                    lo = temp_lo
                    hi = temp_hi
                size = float(size)
                level += 1
                p *= 0.7
                lo_cutoff = size* (p - 0.03)
                hi_cutoff = size* (p + 0.03)
                if level > 3: #Right now we are limiting things to 4 sublevels (including 0 index) <--- maybe change this later?
                    return

                

    
