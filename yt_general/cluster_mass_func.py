import yt
from yt.frontends.sph.data_structures import ParticleDataset
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.axes import Axes #REMOVE?
from scipy import sparse
from astroML.clustering import HierarchicalClustering
from yt.analysis_modules.halo_finding.api import * #Is this using an old model?
from random import randint
import subprocess
import socket
import time

#####
#Note this script requires that "rock_update.py" is located in the same directory
#####

###
#Note that to run this script with a detached screen, it must be run in ipython
#To determine the localhost id, run the command "$echo $DISPLAY" while the screen is still detached
###




screen_setup = True #When set to true, the code functions for a detached screen in linux
ParticleDataset.filter_bbox = True
ParticleDataset._skip_cache = True
file_name = "snapshot_135.hdf5"

plot_title = ''
time_log = []

ds0 = yt.load(file_name)
ds0.index
dd0 = ds0.all_data()
X = dd0['PartType1', 'Coordinates']






##Filter by Bounding_Box##

ii = randint(0, X.shape[0]-1)
c = np.array(X[ii]).copy()
print "Center Index: %s" %str(c)
plot_title += 'c = %s, ' %str(c)
r = 30000.0  #Must be float #100 radius ---> ~30,000 particles
plot_title += 'r = %s, ' %str(r)

if r != 0.0:
    left = ds0.arr((c - (0.5*r)), 'code_length')
    right = ds0.arr((c + (0.5*r)), 'code_length')
    bbox = [[left[0], right [0]], [left[1], right[1]], [left[2], right[2]]]
    ds1 = yt.load(file_name, bounding_box = bbox)
    ds1.periodicity = (False, False, False) #Prevents particle distribution from repeating all the way back to the origin!
    dd1 = ds1.all_data()
    Y = dd1['PartType1', 'Coordinates']

else:
    ds1 = ds0
    dd1 = dd0
    Y = X
    r = float(ds0.domain_width[0])
    c = np.array([r,r,r])*0.5

mass = dd1['PartType1','Masses'].in_units('kg')[0]  # WARNING !!! Assumes a fixed mass for all particles !!!
vol = float(ds1.domain_width.in_units('Mpc')[0])**3.0 #<--- This term for volume is oversimplified, it ignores the potentially empty areas obtained from defining a region outside of the initial dataset, which contains no particles
#A possibly more accurate method would be to use the domain "right_edge" and "left_edge" and use max(left_edge, 0) and min(right_edge, 62k) etc... CHANGE THIS LATER




##MST Cluster Finder##

n_neighbors = 10
edge_cutoff = 0.5
cluster_cutoff = 25
model = HierarchicalClustering(n_neighbors=n_neighbors,
                                   edge_cutoff=edge_cutoff,
                                   min_cluster_size=cluster_cutoff)

temp_time = time.time()
model.fit(Y)
time_log += ['MST: ' + str(time.time()- temp_time) + ' seconds']


labels = list(model.labels_)
#L5 = [] #Count of MST cluster sizes
#for i in range(model.n_components_): #This loop might be too time consuming
#    L5 += [labels.count(i)]
L5 = list(np.zeros(model.n_components_))
for elt in model.labels_:
    if elt != -1:
        L5[elt] += 1


print '0.5 MST Done!'









#Recomputes the MST algorithm for a different edge cutoff value
def MST_edge_adjust(model, edge_cutoff):
    mod_update = model.compute_clusters(edge_cutoff = edge_cutoff)
    #Returns a list with 3 entries
    #0th entry is the new value of model.n_components_
    #1st entry is the new model.labels_ list
    #2nd entry is the new truncated minimum spanning tree
    ##NOTE that the values of the model itself are not updated!!!

    
    N = mod_update[0]
    labels = list(mod_update[1])
    #clust_sizes = [] #Count of MST cluster sizes
    #for i in range(mod_update[0]):
    #    clust_sizes += [labels.count(i)]
    
    clust_sizes = list(np.zeros(N))
    for elt in labels:
        if elt != -1:
            clust_sizes[elt] += 1 

    return clust_sizes







##MST 0.6 edge cutoff version## COPY COPY COPY !!!.
temp_time = time.time()
L6 = MST_edge_adjust(model, 0.6)
time_log += ['MST 0.6 ver: ' + str(time.time()- temp_time) + ' seconds']
print '0.6 MST Done!'


##MST 0.7 edge cutoff version## COPY COPY COPY !!!
temp_time = time.time()
L7 = MST_edge_adjust(model, 0.7)
time_log += ['MST 0.7 ver: ' + str(time.time()- temp_time) + ' seconds']
print '0.7 MST Done!'


##MST 0.8 edge cutoff version## COPY COPY COPY !!!
temp_time = time.time()
L8 = MST_edge_adjust(model, 0.8)
time_log += ['MST 0.8 ver: ' + str(time.time()- temp_time) + ' seconds']
print '0.8 MST Done!'


##MST 0.9 edge cutoff version## COPY COPY COPY !!!
temp_time = time.time()
L9 = MST_edge_adjust(model, 0.9)
time_log += ['MST 0.9 ver: ' + str(time.time()- temp_time) + ' seconds']
print '0.9 MST Done!'


##MST 0.25 edge cutoff version## COPY COPY COPY !!!  <---- REMOVE THIS SECTION LATER
temp_time = time.time()
L10 = MST_edge_adjust(model, 0.25)
time_log += ['MST 0.25 ver: ' + str(time.time()- temp_time) + ' seconds']
print '0.25 MST Done!'


##MST 0.125 edge cutoff version## COPY COPY COPY !!!  <---- REMOVE THIS SECTION LATER
temp_time = time.time()
L11 = MST_edge_adjust(model, 0.125)
time_log += ['MST 0.125 ver: ' + str(time.time()- temp_time) + ' seconds']
print '0.125 MST Done!'




##HOP Cluster Finder##

temp_time = time.time()
hops = HOPHaloFinder (ds = ds1)
time_log += ['HOP: ' + str(time.time()- temp_time) + ' seconds']
L2 = [] #Count of HOP cluster sizes
for elt in hops:
    L2 += [elt.get_size()]





##FoF Cluster Finder##

temp_time = time.time()
fofs = FOFHaloFinder(ds = ds1)
time_log += ['FoF: ' + str(time.time()- temp_time) + ' seconds']
L3 = [] #Count of FoF cluster sizes
for elt in fofs:
    L3 += [elt.get_size()]





##Rockstar Cluster Finder##

temp_time = time.time()
pop = subprocess.Popen("mpirun -n 3 python rock_update.py" + " " + file_name + " " + str(r) + " " + str(c[0]) + " "  + str(c[1]) + " " + str(c[2]), shell = True)
pop.communicate() #Waits for the subprocess to complete in terminal
time_log += ['(Approx) Rockstar: ' + str(time.time()- temp_time) + ' seconds'] #Likely some additional time wasted due to openning a new file and initializing mpi, yt parallelism, etc...

temp_host = socket.gethostname()
host = temp_host.split('.')[0]
rockstar_files = host + "/rockstar_halos/halos_0.0.bin"
rock_halos = yt.load(rockstar_files) #This is the default file where the halo data is saved
rock_set = rock_halos.all_data() #Dataset of rockstar calculated halos 
L4 = []
for i in range(rock_set['halos','particle_mass'].shape[0]):
    L4 += [round(rock_set['halos','particle_mass'].in_units('kg')[i]/mass)]





############Plotting###################
#######################################


def clust_counter(L):#L is a list of all the cluster sizes
    clust_size_freq = np.zeros(Y.shape[0]+1) #Each +1 is one more distinct cluster containing the index # of elements
    for i in set(L):###Could our cluster counting algorithm be off? Or double counting somehow???? <----- CHECK THIS OUT LATER
        j = L.count(i)
        clust_size_freq[int(i)] += j

    #Number of clusters less than N (index value)
    cum_size_freq = np.zeros(clust_size_freq.size)
    temp_sum = 0
    for i in range(clust_size_freq.size -1, -1, -1): #This might be unecessarily time consuming?
        temp_sum += clust_size_freq[i]
        cum_size_freq[i] = temp_sum

    global vol
    return clust_size_freq/vol, cum_size_freq/vol #Returns frequencies and cumulative frequencies per volume



def shift_view(event):
    
    global xmin
    global xmax
    global delta
    global ax
    
    
    if event.key == 'q':
        plt.close()

    if event.key == 'left':
        xmin -= delta
        xmax -= delta
        plt.xlim(xmin, xmax)
        plt.draw()

    if event.key == 'right':
        xmin += delta
        xmax += delta
        plt.xlim(xmin, xmax)
        plt.draw()





L2a, L2b = clust_counter(L2) #HOP clust size freq & cumulative size freq
L3a, L3b = clust_counter(L3) #FoF clust size ...etc...
L4a, L4b = clust_counter(L4) #Rockstar clust size ...etc...
L5a, L5b = clust_counter(L5) #MST 0.5 edge cuttoff clust size ...etc...
L6a, L6b = clust_counter(L6) #MST 0.6 edge cuttoff clust size ...etc...
L7a, L7b = clust_counter(L7) #MST 0.7 edge cuttoff clust size ...etc...
L8a, L8b = clust_counter(L8) #MST 0.8 edge cuttoff clust size ...etc...
L9a, L9b = clust_counter(L9) #MST 0.9 edge cuttoff clust size ...etc...
L10a, L10b = clust_counter(L10) #<----- REMOVE LATER
L11a, L11b = clust_counter(L11) #<----- REMOVE LATER
L12b = L5b + L10b + L11b #REMOVE 


Lmax = [L2b, L3b, L4b, L5b, L6b,  L7b, L8b, L9b, L12b] #Finds the largest cluster size #REMOVE L11b later
Lmaxed = [0] #In case all lists contain only zeros
for elt in Lmax:
    if elt.nonzero()[0].size!=0:
        Lmaxed += [elt.nonzero()[0][-1]]
M = max(Lmaxed) #Largest cluster size overall


xmin = 0
xmax = 0.2*M
delta = (1.0/3.0)*0.2*M



if screen_setup == True:
    #Here we first get the localhost value by using the command "$echo $DISPLAY" before reattaching the screen
    #We then use the number specified for the following command to ensure that the localhost is correct
    k = str(int(raw_input("Done! Input localhost id number: ")))
    subprocess.os.environ['DISPLAY'] = "localhost:"+k+".0"



#Plot of cluster size
fig, ax = plt.subplots()
fig.canvas.mpl_connect('key_press_event', shift_view)
N = np.array(range(Y.shape[0]+1))*mass  #For fixed mass, N is proportional to total mass of each cluster
plt.plot(N, L2b, color = 'g', label = 'HOP')
plt.plot(N, L3b, color = 'k', label = 'FoF')
plt.plot(N, L4b, color = 'r', label = 'Rockstar')
plt.plot(N, L5b, color = 'b', linestyle = 'dashed', label = '0.5 MST')
plt.plot(N, L6b, color = 'r', linestyle = 'dashed', label = '0.6 MST')
plt.plot(N, L7b, color = 'g', linestyle = 'dashed', label = '0.7 MST')
plt.plot(N, L8b, color = 'k', linestyle = 'dashed', label = '0.8 MST')
plt.plot(N, L9b, color = 'y', linestyle = 'dashed', label = '0.9 MST')
plt.plot(N, L12b, color = 'brown', linestyle = 'dashed', label = 'Subhalo MST') #REMOVE
plt.legend()
plt.suptitle(plot_title, fontweight = 'bold', fontsize = 24)
plt.xlabel('log M')
plt.ylabel('Log Num Clusters w/ Size $\geq$ N per $Mpc^{3}$')
plt.xscale('log')
plt.yscale('log')
plt.show(block=False)


xmin = 0
xmax = 0.2*M
fig, ax = plt.subplots()
ax.autoscale(axis = 'y')
fig.canvas.mpl_connect('key_press_event', shift_view)
plt.plot(N, L2b, color = 'g', label = 'HOP')
plt.plot(N, L3b, color = 'k', label = 'FoF')
plt.plot(N, L4b, color = 'r', label = 'Rockstar')
plt.plot(N, L5b, color = 'b', linestyle = 'dashed', label = '0.5 MST')
plt.plot(N, L6b, color = 'r', linestyle = 'dashed', label = '0.6 MST')
plt.plot(N, L7b, color = 'g', linestyle = 'dashed', label = '0.7 MST')
plt.plot(N, L8b, color = 'k', linestyle = 'dashed', label = '0.8 MST')
plt.plot(N, L9b, color = 'y', linestyle = 'dashed', label = '0.9 MST')
plt.plot(N, L12b, color = 'brown', linestyle = 'dashed', label = 'Subhalo MST') #REMOVE
plt.legend()
plt.suptitle(plot_title, fontweight = 'bold', fontsize = 24)
plt.xlabel('M')
plt.ylabel('Num Clusters w/ Size $\geq$ N per $Mpc^{3}$')
plt.show(block=False)





##Time Log##
print 'Time taken...'
for elt in time_log:
    print elt

print 'max L11', max(L11)
