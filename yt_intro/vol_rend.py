import yt
ds = yt.load("IsolatedGalaxy/galaxy0030/galaxy0030")

tf = yt.ColorTransferFunction((-28, -24))
tf.add_layers(4, w=0.01)
cam = ds.camera([0.5, 0.5, 0.5], [1.0, 1.0, 1.0], (20, 'kpc'), 512, tf, fields=["density"])
cam.show()
#Doesn't work but it isn't particularly important
