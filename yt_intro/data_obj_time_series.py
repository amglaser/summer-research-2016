import yt
import numpy as np
from matplotlib import pylab
from yt.analysis_modules.halo_finding.api import HaloFinder

ts = yt.DatasetSeries("enzo_tiny_cosmology/*/*.hierarchy") #asterisk is glob pattern

"""
rho_ex = []
times = []
for ds in ts:
    dd = ds.all_data()
    rho_ex.append(dd.quantities.extrema("density"))
    times.append(ds.current_time.in_units("Gyr"))
rho_ex = np.array(rho_ex)



pylab.semilogy(times, rho_ex[:,0], '-xk', label='Minimum') #all rows, 0th column entry of matrix
pylab.semilogy(times, rho_ex[:,1], '-xr', label='Maximum')
pylab.ylabel("Density ($g/cm^3$)")
pylab.xlabel("Time (Gyr)")
pylab.legend()
pylab.ylim(1e-32, 1e-21)
pylab.show()
"""


from yt.units import Msun

"""
mass = []
zs = []
for ds in ts:
    halos = HaloFinder(ds)
    dd = ds.all_data()
    total_mass = dd.quantities.total_quantity("cell_mass").in_units("Msun")
    total_in_baryons = 0.0*Msun
    for halo in halos:
        sp = halo.get_sphere()
        total_in_baryons += sp.quantities.total_quantity("cell_mass").in_units("Msun")
    mass.append(total_in_baryons/total_mass)
    zs.append(ds.current_redshift)

pylab.semilogx(zs, mass, '-xb')
pylab.xlabel("Redshift")
pylab.ylabel("Mass in halos / Total mass")
pylab.xlim(max(zs), min(zs))
pylab.ylim(-0.01, .18)
pylab.show()
"""

ds = yt.load("IsolatedGalaxy/galaxy0030/galaxy0030")
cp = ds.cutting([0.2, 0.3, 0.5], "max")
pw = cp.to_pw(fields = [("gas", "density")])
pw.show() #Doesn't work. Instead use the image_update function in the simple_viz.py file
print pw.plots.keys()
