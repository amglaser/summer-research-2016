download_datasets = True
if download_datasets:
    get_ipython().system(u'curl -sSO http://yt-project.org/data/enzo_tiny_cosmology.tar.gz')
    print "Got enzo_tiny_cosmology"
    get_ipython().system(u'tar xzf enzo_tiny_cosmology.tar.gz')
    
    get_ipython().system(u'curl -sSO http://yt-project.org/data/Enzo_64.tar.gz')
    print "Got Enzo_64"
    get_ipython().system(u'tar xzf Enzo_64.tar.gz')
    
    get_ipython().system(u'curl -sSO http://yt-project.org/data/IsolatedGalaxy.tar.gz')
    print "Got IsolatedGalaxy"
    get_ipython().system(u'tar xzf IsolatedGalaxy.tar.gz')
    
    print "All done!"
