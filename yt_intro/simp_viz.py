import yt
from PIL import Image

ds = yt.load("enzo_tiny_cosmology/DD0046/DD0046")
print ("Redshift =", ds.current_redshift)


p = yt.ProjectionPlot(ds, "y", "density")
plot_name = "viz_test"


def image_update(plot, filename): #Note, filename only specifies what the file names start with for multiple plots
    files = plot.save(filename)
    for elt in files:
        img = Image.open(elt)
        img.show()


##(Can also use "eog <image file name>" in terminal to view images)##

"""
image_update(p, plot_name)

p.zoom(2.0)
image_update(p,plot_name)

p.pan_rel((0.1,0.0))
image_update(p,plot_name)
"""

p = yt.ProjectionPlot(ds, "z", ["density", "temperature"], weight_field="density")
#image_update(p, plot_name)


ds = yt.load("Enzo_64/DD0043/data0043")
s = yt.SlicePlot(ds, "z", ["density", "velocity_magnitude"], center="max")
s.set_cmap("velocity_magnitude", "kamae")
s.zoom(10.0)
#image_update(s, plot_name)

s.annotate_velocity()
#image_update(s, plot_name)


s = yt.SlicePlot(ds, "x", ["density"], center="max")
s.annotate_contour("temperature") #There are several different kinds of annotations
s.zoom(2.5)
image_update(s, plot_name)


