import yt
import numpy as np
from yt import derived_field
from matplotlib import pylab


@derived_field(name = "dinosaurs", units = "K*cm/s")
def _dinos(field, data):
    return data["temperature"]*data["velocity_magnitude"]

#Derived fields must be defined BEFORE any datasets are loaded

ds = yt.load("IsolatedGalaxy/galaxy0030/galaxy0030")
dd = ds.all_data()
print dd.quantities.keys()

print dd.quantities.extrema("dinosaurs")

print dd.quantities.weighted_average_quantity("dinosaurs", weight="temperature")


sp = ds.sphere("max", (10.0, 'kpc'))
bv = sp.quantities.bulk_velocity()
L = sp.quantities.angular_momentum_vector()
rho_min, rho_max = sp.quantities.extrema("density")
print bv
print L
print rho_min, rho_max


prof = yt.Profile1D(sp, "density", 32, rho_min, rho_max, True, weight_field="cell_mass")
prof.add_fields(["temperature","dinosaurs"])
pylab.loglog(np.array(prof.x), np.array(prof["temperature"]), "-x")
pylab.xlabel('Density $(g/cm^3)$')
pylab.ylabel('Temperature $(K)$') #Note, uses latex units!


pylab.loglog(np.array(prof.x), np.array(prof["dinosaurs"]), '-x')
pylab.xlabel('Density $(g/cm^3)$')
pylab.ylabel('Dinosaurs $(K cm / s)$')


prof = yt.Profile1D(sp, "density", 32, rho_min, rho_max, True, weight_field=None)
prof.add_fields(["cell_mass"])
pylab.loglog(np.array(prof.x), np.array(prof["cell_mass"].in_units("Msun")), '-x')
pylab.xlabel('Density $(g/cm^3)$')
pylab.ylabel('Cell mass $(M_\odot)$')


prof = yt.ProfilePlot(sp, 'density', 'cell_mass', weight_field=None)
prof.set_unit('cell_mass', 'Msun')
prof.show()



sp_small = ds.sphere("max", (50.0, 'kpc'))
bv = sp_small.quantities.bulk_velocity()

sp = ds.sphere("max", (0.1, 'Mpc'))
rv1 = sp.quantities.extrema("radial_velocity")

sp.clear_data()
sp.set_field_parameter("bulk_velocity", bv)
rv2 = sp.quantities.extrema("radial_velocity")

print bv
print rv1
print rv2
